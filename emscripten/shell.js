"use strict";

let commands = [];

const input = document.getElementById("command");
const output = document.getElementById("output");

const setStatus = (text) => console.info(`status: ${text}`);

const setupCommandInput = () => {
  input.setAttribute("data-command-id", -1);

  input.onkeypress = (event) => {
    if (event.key !== "Enter") {
      return;
    }

    commands.push(input.value.trim());
    input.setAttribute("data-command-id", -1);
    input.value = "";

    run();
  };

  input.onkeydown = (event) => {
    const commandId = parseInt(input.getAttribute("data-command-id"));

    // Two-pass so that `["ls", "", "", "ls"]` returns `["ls"]`
    let commandsFiltered = commands
      .filter((command) => command !== "")
      .filter((command, index, commands) => {
        const isFirstCommand = index === 0;
        const isPreviousCommand = command === commands[index - 1];

        return isFirstCommand || !isPreviousCommand;
      });

    if (event.key === "ArrowUp") {
      // Prevent the default "jump to beginning of text"
      event.preventDefault();

      // No previous command
      if (commandsFiltered.length === 0 || commandId === 0) {
        return;
        // Set to most recently executed command
      } else if (commandId === -1) {
        input.value = commandsFiltered.at(-1) || "";
        input.setAttribute("data-command-id", commandsFiltered.length - 1);
        // Set to previous command
      } else {
        const prevCommandId = commandId - 1;
        input.value = commandsFiltered.at(prevCommandId);
        input.setAttribute("data-command-id", prevCommandId);
      }
    } else if (event.key === "ArrowDown") {
      if (commandId === commandsFiltered.length - 1) {
        input.value = "";
        input.setAttribute("data-command-id", -1);
        // No next command
      } else if (commandsFiltered.length === 0 || commandId === -1) {
        return;
        // Set to next command
      } else {
        const nextCommandId = commandId + 1;
        input.value = commandsFiltered.at(nextCommandId);
        input.setAttribute("data-command-id", nextCommandId);
      }
    }
  };
};

setupCommandInput();

let outputBuffer = "";
const appendOutput = (text, clear = false) => {
  outputBuffer += text;

  const flushOutput = () => {
    if (outputBuffer.length === 0) {
      return;
    }

    if (clear) {
      output.value = "";
    }

    output.value += outputBuffer;
    outputBuffer = "";

    // Auto-scroll to bottom
    output.scrollTop = output.scrollHeight;
  };

  // Since stdout and stderr are character-by-character,
  // buffer the output for speed
  // This is especially noticeable with `help` which
  // outputs 2000+ characters under a default configuration
  // (change `setTimeout` to `flushOutput()` to observe)
  // This works because JavaScript is single-threaded
  // and all of the `appendOutput`s will be called before `flushOutput()`
  setTimeout(flushOutput, 0);
};

const run = () => {
  const moduleSettings = {
    // Since the all entered commands are re-run on each command,
    // only show exit message when it is an abnormal exit
    onExit: (statusCode) =>
      statusCode ? appendOutput(`<program exit: ${statusCode}>`) : null,
    postRun: [() => setStatus("program ended")],
    preRun: [
      () => {
        setStatus("setting up filesystem");

        moduleSettings.FS.mkdir("/etc", 0o755);

        // Fix commands that read `/etc/passwd`, such as `whoami`
        moduleSettings.FS.writeFile(
          "/etc/passwd",
          "web_user:x:0:0:emscripten:/home/web_user:/usr/sbin/nologin"
        );
      },
      () => {
        setStatus("setting up environment variables");

        // Display ASCII characters instead of UTF-8
        // (for example, by `tree`)
        moduleSettings.ENV.LC_ALL = "ASCII";
      },
      () => {
        setStatus("setting up module");

        // Rather than setting `output.value` directly, use a buffered output
        // to avoid flash of white (time between text below displayed,
        // and when program output displayed)
        appendOutput(
          "For available shell commands, run the built-in `help` command.\n" +
            "For sample commands, view the repository's documentation.\n",
          true
        );

        let i = 0;
        function stdin() {
          // `exit 0` at the end to make it clear the program is re-run
          // in its entirety every time a new command is entered
          const input = ["cd /home/web_user", ...commands, "exit 0"].reduce(
            (accumulator, command) => `${accumulator}${command}\n`,
            ""
          );

          if (i < input.length) {
            const code = input.charCodeAt(i);

            i++;
            return code;
          } else {
            return null;
          }
        }

        const stdout = (code) => appendOutput(String.fromCharCode(code));

        let stderrBuffer = "";
        const stderr = (code) => {
          if (code === "\n".charCodeAt(0)) {
            appendOutput(`\n> ${stderrBuffer}\n`);
            stderrBuffer = "";
          } else {
            stderrBuffer += String.fromCharCode(code);
          }
        };

        moduleSettings.FS.init(stdin, stdout, stderr);
      },
    ],
    totalDependencies: 0,
    monitorRunDependencies: (remaining) => {
      this.totalDependencies = Math.max(this.totalDependencies, remaining);
      setStatus(
        remaining
          ? `preparing dependencies (${this.totalDependencies - remaining} / ${
              this.totalDependencies
            })`
          : "all dependencies loaded"
      );
    },
  };

  createModule(moduleSettings).then((mainModule) => {
    mainModule.callMain(["ash", "-v"]);
  });
};

run();
