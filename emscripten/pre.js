// https://emscripten.org/docs/api_reference/module.html#creating-the-module-object
// https://emscripten.org/docs/tools_reference/emcc.html#emcc-pre-js

// BusyBox needs to be called as `busybox ash` to open Almquist shell
Module["thisProgram"] = "busybox";
