- `filesystem/`
    - Runtime filesystem, as seen by BusyBox Web
- `pre.js`
    - Sets `argv[0]` to `busybox` so that it properly detects which applet to run
- `shell.{css,html,js}`
    - [`emcc`'s `--shell-file` option](https://emscripten.org/docs/tools_reference/emcc.html#emcc-shell-file)
      requires the specified file to contain `{{{ SCRIPT }}}`,
      which adds `<script async src="main.js"></script>`.
      Since `async` is not needed [as a module](https://github.com/emscripten-core/emscripten/blob/3.1.35/src/settings.js#L1230),
      `shell.html` is used instead.
