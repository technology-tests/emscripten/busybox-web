#!/usr/bin/env bash

# Since WASM does not support `exec` and `fork`,
# remove those calls from BusyBox
# This may break some programs, but works better for a demo

# For example, `ls` (APPLET_NOEXEC) crashes the runtime when used as-is,
# but works fine when changed to APPLET_NOFORK

# `ls` is NOEXEC (https://git.busybox.net/busybox/tree/NOFORK_NOEXEC.lst?h=1_36_0#n221)
# because it is classified as a runner (https://git.busybox.net/busybox/tree/NOFORK_NOEXEC.lst?h=1_36_0#n13)
# This is safe to convert to NOFORK, provided that ctrl+C does not need to work
# (ctrl+C in the web demo is the usual "copy" functionality, not SIGINT, so it is safe to convert)

# From `APPLET_NOEXEC(...)` to `APPLET_NOFORK(...)`
find . -name '*.c' -exec sed -E -i 's/APPLET_NOEXEC\(/APPLET_NOFORK(/' {} +

# From `APPLET(name, ...)` to `APPLET_NOFORK(name, name, ..., name)`
find . -name '*.c' -exec sed -E -i 's/APPLET\(([^,]+),([^)]+)\)/APPLET_NOFORK(\1,\1,\2,\1)/' {} +

# Note:
#   the two `find` commands can be converted into a single one,
#   but I find it less clear
