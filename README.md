# BusyBox Web

[BusyBox](https://busybox.net/) ported to the web with [Emscripten](https://emscripten.org/).
Runs locally within your browser.
BusyBox Web is not expected to work properly in some cases due to limitations of WASM.

## Live Demo

The demo link can be found in the project description.

Since Emscripten requires input to be synchronous by default,
we work around this by starting a new instance each time a command is entered and re-run all commands.
For example, `echo $RANDOM` will likely display a different value with each new command.

### Sample run

```console
> echo "Shell redirection works too" > demo.txt

> ls -alh
total 2K
drwxrwxrwx    1 web_user 0           4.0K Jan 1 00:00 .
drwxrwxrwx    1 web_user 0           4.0K Jan 1 00:00 ..
-rw-rw-rw-    1 web_user 0             28 Jan 1 00:00 demo.txt

> cat demo.txt
Shell redirection works too

> tree /
/
|-- dev
|   |-- null
|   |-- random
|   |-- shm
|   |   `-- tmp
|   |-- stderr
|   |-- stdin
|   |-- stdout
|   |-- tty
|   |-- tty1
|   `-- urandom
|-- etc
|   `-- passwd
|-- home
|   `-- web_user
|       `-- demo.txt
|-- proc
|   `-- self
|       `-- fd
`-- tmp

9 directories, 12 files

> exit 0
```

For a more comprehensive list of commands, [view `docs/commands.md`](https://gitlab.com/technology-tests/emscripten/busybox-web/-/blob/main/docs/commands.md).


## Overview

- Compiled with `emcc` instead of `gcc` or `clang`
- `fork`s and `exec`s removed from BusyBox's source code
    - Many BusyBox applets start a new process, which is not allowed by WASM.
      To get around this, all applets run in the same process,
      which causes unexpected behaviour in some cases
- Custom CSS/HTML/JS-based terminal, which supports browser shortcuts
    - For example, <kbd>ctrl</kbd>+<kbd>a</kbd> selects whole command
      instead of jumping to beginning of line
    - Command history can be cycled through with up/down arrow keys
