# Preface

This document lists available commands and sample usages for BusyBox Web.
For command line options, [view the generated documentation][BusyBox Docs].

Due to the lack of support for `exec` and `fork` calls in compiled Emscripten WebAssembly,
BusyBox Web has been modified from [the original BusyBox](https://busybox.net/) to not use those calls.
Workarounds for non-standard behaviour are described below.


# Workarounds

## Pipes

Instead of piping from one command to another directly,
save the output to a file and use shell redirection to
send the input into the second command.

- Before: `echo "Hello, World!" | md5sum`
- After: `echo "Hello, World!" > stdout.out; md5sum - < stdout.out`


## `exec`s and `fork`s

No manual workaround required. BusyBox Web has already been patched to avoid the use of these calls.
[View `busybox-patches/convert-to-nofork.sh`][convert-to-nofork.sh] for details.


## Hangs

To restart BusyBox Web, simply reload the page.


## Overwritten process state

Because all applets are run in the same process,
process state will be overwritten.
This may cause BusyBox Web to exhibit abnormal behaviour:

```console
> cd /home

> ls
web_user

> tree / > /dev/null

> ls
dev
etc
home
proc
tmp

> pwd
/home
```

After `tree` is run, `ls` is listing contents of `/` instead of `/home`.
To fix this issue, `cd /home` again before `ls`, or explicitly pass a path: `ls /home`.


# Commands

## Shell built-ins

- `.`
    - Example: `. /home/web_user/script.sh`
- `:`
    - Example: `: "null command, does nothing except returning true"`
- `[`
    - Example: `[ 1 -eq 1 ] && echo "They are equal" || echo "They are not equal"`
- `[[`
    - Example: `[[ 5 =~ ^[0-9]$ ]] && echo "Input is a digit" || echo "Input is not a digit"`
- `alias`
    - Example: `alias demo_echo='echo "Hello, World!"'` followed by `demo_echo`
- `break`
    - Example: `while true; do echo Hello; break; done`
- `cd`
    - Example: `cd / && ls`
- `chdir`
    - Example: `chdir /etc && ls`
- `command`
    - Example: `command ls`
- `continue`
    - Example: `for i in 1 2 3 4 5; do if [ $i -eq 2 ]; then continue; fi; echo $i; done`
- `echo`
    - Example: `echo "Hello, World!"`
- `eval`
    - Example: `eval echo '$'USER`
- `exit`
    - Example: `exit 1`
- `false`
    - Example: `false; echo $?`
- `getopts`
    - Example: `. /home/web_user/getopts.sh -a -b 5`
        - Note: such scripts are usually run with `sh getopts.sh <arguments>`,
          but that would not work with BusyBox Web because it requires a separate process
- `help`
    - Example: `help`
- `let`
    - Example: `let "val = (3 * 2 - 1) ** 2"; echo $val`
- `local`
    - Example: `function demo_local() { global_value=5; local local_value=3; echo "Inside function: ${local_value:-NA},$global_value"; }; demo_local; echo "Outside function: ${local_value:-NA},$global_value"`
- `printf`
    - Example: `printf "%s: %0.2f\n" "Value" 5`
- `pwd`
    - Example: `pwd`
- `read`
    - Example: `read demo_read < /etc/passwd; echo "$demo_read"`
- `readonly`
    - Example: `readonly demo_readonly=10; demo_readonly=5`
- `return`
    - Example: `function demo_return() { return 2; }; demo_return; echo "Exit code: $?"`
- `set`
    - Example: `set -e; false; echo "Still running"`
        - With `set -e`, BusyBox Web exits upon the first error (`false`)
- `shift`
    - Example: `function demo_shift() { echo $1; shift; echo $1; }; demo_shift a b`
- `sleep`
    - Example: `sleep 2s`
- `source`
    - Alias of `.`
- `test`
    - Alias of `[`
- `true`
    - Example: `true; echo $?`
- `type`
    - Example: `type true; type ls`
- `unalias`
    - Example: `alias a="echo Hello"; unalias a` followed by `a`
        - Note: `alias`es aren't processed until the next line,
          so separate into two commands in case user tries to
          remove `unalias` for comparing results
- `unset`
    - Example: `val=3; echo "Value: ${val:-NA}"; unset val; echo "Value: ${val:-NA}"`


## BusyBox Applets

- `addgroup`
    - Example: `addgroup new_group; cat /etc/group`
- `arch`
    - Example: `arch`
- `ascii`
    - Example: `ascii`
- `ash`
    - Example: `ash --help`
    - Note: this is BusyBox Web's entrypoint, so it must be included
      even though a new shell cannot be started interactively (because of WASM limitations)
- `awk`
    - Example: `awk 'NR > 3 { print $NF }' /home/web_user/poem.txt`
    - Note: hangs after the command completes. Refresh the page to continue
- `basename`
    - Example: `basename /home/web_user/poem.txt`
- `bc`
    - Example: `bc -q /home/web_user/equation.bc`
- `cal`
    - Example: `cal`
- `cat`
    - Example: `cat /home/web_user/poem.txt`
- `chgrp`
    - Example: `chgrp -c www-data /home/web_user/poem.txt`
- `chmod`
    - Example: `chmod -v 700 /home/web_user/poem.txt`
- `chown`
    - Example: `chown -v 1000 /home/web_user/poem.txt`
- `cksum`
    - Example: `cksum /home/web_user/poem.txt`
- `cmp`
    - Example: `cmp /home/web_user/github.txt /home/web_user/gitlab.txt`
- `comm`
    - Example: `comm /home/web_user/comm-a.txt /home/web_user/comm-b.txt`
- `cp`
    - Example: `cp /home/web_user/poem.txt /home/web_user/poem-copy.txt`
- `crc32`
    - Example: `crc32 /home/web_user/poem.txt`
- `cryptpw password salt`
    - Example: `cryptpw password salt`
- `cut`
    - Example: `cut -F 1 /home/web_user/poem.txt`
- `date`
    - Example: `date -R -d '15:00'`
- `dc`
    - Example: `dc -e '[2^10 =] p' -e '2 10 ^ p'`
- `delgroup`
    - Example: `delgroup web_user`
- `df`
    - Example: `df -h`
- `diff`
    - Example: `diff /home/web_user/comm-a.txt /home/web_user/comm-b.txt`
- `dirname`
    - Example: `dirname /home/web_user/poem.txt`
- `dos2unix`
    - Example: `hd /home/web_user/script.sh; dos2unix -d /home/web_user/script.sh; hd /home/web_user/script.sh`
- `du`
    - Example: `du -c /dev`
- `env`
    - Example: `env`
- `expand`
    - Example: `expand -t 2 /home/web_user/getopts.sh`
- `expr`
    - Example: `expr 3 '*' 5 % 2`
- `factor`
    - Example: `factor 52`
- `fallocate`
    - Example: `fallocate -l 512 /tmp/allocated; ls -alh /tmp/allocated`
- `find`
    - Example: `find /home -name 'poem.txt'`
- `fold`
    - Example: `fold -s -w 20 /home/web_user/poem.txt`
- `fsync`
    - Example: `fsync /home/web_user/poem.txt`
- `grep`
    - Example: `grep -o -n -E '\b[^ ]{7}\b' /home/web_user/poem.txt`
- `groups`
    - Example: `groups`
- `hd`
    - Example: `hd /home/web_user/script.sh`
- `head`
    - Example: `head -2 /home/web_user/poem.txt`
- `hexdump`
    - Example: `hexdump -c /home/web_user/script.sh`
- `hostid`
    - Example: `hostid`
- `hostname`
    - Example: `hostname -i`
- `httpd`
    - Example: `httpd -e 'https://example.com/file?key=value#id'`
- `id`
    - Example: `id`
- `install`
    - Example: `install /home/web_user/poem.txt /tmp`
- `logname`
    - Example: `logname`
- `ls`
    - Example: `ls -alh /home/web_user`
- `makemime`
    - Example: `makemime /home/web_user/poem.txt`
- `md5sum`
    - Example: `md5sum /home/web_user/poem.txt`
- `mkdir`
    - Example: `mkdir -p a/b/c; ls -alhR a`
- `mknod`
    - Example: `mknod -m 0666 null c 1 3`
- `mkpasswd`
    - Example: `mkpasswd password salt`
- `mktemp`
    - Example: `mktemp`
- `mountpoint`
    - Example: `mountpoint /`
- `mv`
    - Example: `mv . .`
- `nl`
    - Example: `nl -v 10 -i 10 /home/web_user/poem.txt`
- `od`
    - Example: `od /home/web_user/poem.txt`
- `paste`
    - Example: `paste /home/web_user/poem.txt /home/web_user/poem.txt`
- `printenv`
    - Example: `printenv`
- `realpath`
    - Example: `realpath ../../../../../home/web_user`
- `rev`
    - Example: `rev /home/web_user/poem.txt`
- `rm`
    - Example: `rm -rf /home/web_user`
- `rmdir`
    - Example: `rmdir /tmp`
- `sed`
    - Example: `sed 's/executed/completed/' /home/web_user/script.sh`
- `seq`
    - Example: `seq -s " " -w 1 100`
- `sha1sum`
    - Example: `sha1sum /home/web_user/poem.txt`
- `sha256sum`
    - Example: `sha256sum /home/web_user/poem.txt`
- `sha3sum`
    - Example: `sha3sum /home/web_user/poem.txt`
- `sha512sum`
    - Example: `sha512sum /home/web_user/poem.txt`
- `shuf`
    - Example: `shuf -n 5 /home/web_user/poem.txt`
- `sort`
    - Example: `sort /home/web_user/poem.txt`
- `stat`
    - Example: `stat /dev/null`
- `strings`
    - Example: `head -c 1024 /dev/urandom > random.bin; strings -n 5 random.bin`
- `sum`
    - Example: `sum /home/web_user/poem.txt`
- `tac`
    - Example: `tac /home/web_user/poem.txt`
- `tail`
    - Example: `tail -5 /home/web_user/poem.txt`
- `tee`
    - Example: `tee demo_tee.log < /home/web_user/poem.txt; ls -alh demo_tee.log`
- `touch`
    - Example: `touch -d '2023-01-01 00:00:00' demo_touch; ls -alh demo_touch`
- `tr`
    - Example: `tr 'A-Za-z' 'N-ZA-Mn-za-m' < /home/web_user/poem.txt`
- `tree`
    - Example: `tree /`
- `truncate`
    - Example: `truncate -s 128 demo_truncate; ls -alh demo_truncate`
- `ts`
    - Example: `ts < /home/web_user/poem.txt`
- `tsort`
    - Example: `tsort < /home/web_user/tsort.txt`
- `ttysize`
    - Example: `ttysize`
- `uname`
    - Example: `uname -a`
- `unix2dos`
    - Example: `hd /home/web_user/script.sh; unix2dos -d /home/web_user/script.sh; hd /home/web_user/script.sh`
- `unlink`
    - Example: `touch demo_unlink; unlink demo_unlink; ls -alh demo_unlink`
- `usleep`
    - Example: `usleep 250000; echo "Slept for 0.25 second"`
- `wc`
    - Example: `wc /home/web_user/poem.txt`
- `whoami`
    - Example: `whoami`
- `xxd`
    - Example: `xxd -g 1 /home/web_user/poem.txt`


[BusyBox Docs]: https://technology-tests.gitlab.io/emscripten/busybox-web/docs/BusyBox.html
[convert-to-nofork.sh]: https://gitlab.com/technology-tests/emscripten/busybox-web/-/blob/main/busybox-patches/convert-to-nofork.sh
