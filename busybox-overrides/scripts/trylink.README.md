It it is easier to modify `trylink` and replace the original
than updating a `diff` when developing.

The `diff` shown below is from [BusyBox's `trylink`](https://git.busybox.net/busybox/tree/scripts/trylink?h=1_36_0)
to the patched version that works with Emscripten (`trylink` in this repository).

The changes have been kept to a minimum.
For example, the inconsistent indentation is kept
and no attempt is made at reducing the number of links
(the original script starts with a list of potential libraries to link in,
and removes them one-by-one until it fails, or there is no library remaining,
then performs a final link with a minimal set of libraries linked in).

The linker flag `--warn-common` has been removed as Emscripten's linker
does not understand that flag and causes an error.


```diff
@@ -1,4 +1,17 @@
-#!/bin/sh
+#!/bin/bash
+
+COMPILE_ARGS=(
+   # For embedding files, see https://emscripten.org/docs/porting/files/packaging_files.html#modifying-file-locations-in-the-virtual-file-system
+   '--embed-file' '../emscripten/filesystem/@/'
+   '--pre-js' '../emscripten/pre.js'
+
+   # For settings, see https://github.com/emscripten-core/emscripten/blob/3.1.35/src/settings.js
+   '-s' 'EXIT_RUNTIME=1'
+   '-s' 'EXPORT_NAME=createModule'
+   '-s' 'EXPORTED_RUNTIME_METHODS=callMain,ENV,FS'
+   '-s' 'INVOKE_RUN=0'
+   '-s' 'MODULARIZE=1'
+)

 #debug=true
 debug=false
@@ -42,7 +55,7 @@ try() {
     printf "%s\n" "$*" >>$EXE.out
     printf "%s\n" "==========" >>$EXE.out
     $debug && echo "Trying: $*"
-    $@ >>$EXE.out 2>&1
+    "$@" >>$EXE.out 2>&1
     return $?
 }

@@ -72,7 +85,7 @@ check_libc_is_glibc() {
     return $exitcode
 }

-EXE="$1"
+EXE="$1.js"
 CC="$2"
 CFLAGS="$3"
 LDFLAGS="$4"
@@ -96,7 +109,7 @@ fi
 START_GROUP="-Wl,--start-group"
 END_GROUP="-Wl,--end-group"
 INFO_OPTS() {
-   echo "-Wl,--warn-common -Wl,-Map,$EXE.map -Wl,--verbose"
+   echo "-Wl,-Map,$EXE.map -Wl,--verbose"
 }

 # gold may not support --sort-common (yet)
@@ -135,6 +148,7 @@ echo "Trying libraries: $LDLIBS"
 l_list=`echo " $LDLIBS $CONFIG_EXTRA_LDLIBS " | sed -e 's: \([^- ][^ ]*\): -l\1:g' -e 's/^ *//'`
 test x"$l_list" != x"" && l_list="$START_GROUP $l_list $END_GROUP"
 try $CC $CFLAGS $LDFLAGS \
+   "${COMPILE_ARGS[@]}" \
    -o $EXE \
    $SORT_COMMON \
    $SORT_SECTION \
@@ -162,6 +176,7 @@ while test "$LDLIBS"; do
    test x"$l_list" != x"" && l_list="$START_GROUP $l_list $END_GROUP"
    $debug && echo "Trying -l options: '$l_list'"
    try $CC $CFLAGS $LDFLAGS \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        $SORT_COMMON \
        $SORT_SECTION \
@@ -191,6 +206,7 @@ test x"$l_list" != x"" && l_list="$START_GROUP $l_list $END_GROUP"
 # --verbose gives us gobs of info to stdout (e.g. linker script used)
 if ! test -f busybox_ldscript; then
     try $CC $CFLAGS $LDFLAGS \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        $SORT_COMMON \
        $SORT_SECTION \
@@ -221,6 +237,7 @@ else
     # to unconditionally align .data to the next page boundary,
     # instead of "next page, plus current offset in this page"
     try $CC $CFLAGS $LDFLAGS \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        $SORT_COMMON \
        $SORT_SECTION \
@@ -253,6 +270,7 @@ if test "$CONFIG_BUILD_LIBBUSYBOX" = y; then

     EXE="$sharedlib_dir/libbusybox.so.${BB_VER}_unstripped"
     try $CC $CFLAGS $LDFLAGS \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        -shared -fPIC $LBB_STATIC \
        -Wl,--enable-new-dtags \
@@ -277,6 +295,7 @@ fi
 if test "$CONFIG_FEATURE_SHARED_BUSYBOX" = y; then
     EXE="$sharedlib_dir/busybox_unstripped"
     try $CC $CFLAGS $LDFLAGS \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        $SORT_COMMON \
        $SORT_SECTION \
@@ -315,6 +334,7 @@ int main(int argc, char **argv)

    EXE="$sharedlib_dir/$name"
    try $CC $CFLAGS $LDFLAGS "$sharedlib_dir/applet.c" \
+       "${COMPILE_ARGS[@]}" \
        -o $EXE \
        $SORT_COMMON \
        $SORT_SECTION \
```
