FROM gcc:12.2.0

WORKDIR /

# Files are needed at runtime, so do not `rm -rf emsdk`
RUN git clone --depth=1 --branch=3.1.35 https://github.com/emscripten-core/emsdk.git \
 && cd emsdk \
 && ./emsdk help \
 && ./emsdk list \
 && ./emsdk install 3.1.35 \
 && ./emsdk activate latest
